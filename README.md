When you are looking for some smart LED lights for your rooms, Ecolor is always one of the best options that you can select smart lights from.

## Ecolor Products
Do you know what lights do Ecolor provide? Ecolor has main stream LED lights and is designing more funny and functional smart lights for customers. At present, you can find strip lights, string lights, smart bulbs, table lamps, and others on [Ecolor]( https://www.ecolorled.com/) official site. 

Ecolor strip lights can be used in anywhere you like. For example, you can use the 16.8ft RGB strip lights indoor by sticking them under the cabinet. And you can use Ecolor neon flex outdoor strip lights outdoor even in a rainy weather. 

Now Ecolor has designed a fairy LED light, that’s pearl string light. Ecolor pearl shaped string lights are advanced in setting color. With the Ecolor Life app on your mobile device, you can DIY the colors to make it displays any color you like. The smart string light supports max 5 colors to display at the same time.

Ecolor also has many ambient lights for setting rooms’ atmosphere. For example, Ecolor moon table lamp is a perfect light for bedroom. You can use it for various purposes by changing the light color, brightness, and other features.

## Ecolor Mission
Making your home smarter and your life more convenient is Ecolor’s mission. You can find the desired lights on Ecolor and decorate your house in the way you want.
